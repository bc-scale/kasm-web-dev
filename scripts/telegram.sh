#!/bin/bash
set -ex

add-apt-repository ppa:atareao/telegram
apt-get update
apt-get install -y telegram
