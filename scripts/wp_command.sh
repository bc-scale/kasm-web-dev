#!/bin/bash
set -ex

wget -q https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar -O /usr/bin/wp-cli 
chmod a+x /usr/bin/wp-cli
